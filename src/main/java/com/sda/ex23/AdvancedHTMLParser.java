package com.sda.ex23;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public class AdvancedHTMLParser extends HTMLParser {
    private Logger logger = LogManager.getLogger(AdvancedHTMLParser.class);
    private DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    public AdvancedHTMLParser(String fileName) {
        super(fileName);
    }

    public void parseHTML() {
        logger.info("[Parsing using text search]");
        try {
            List<String> lines = Files.readAllLines(calculatePath());

            List<Article> articles = new ArrayList<>();
            String currentTitle = "";
            String currentContent = "";
            LocalDateTime currentDate = null;
            boolean isContentRead = false;

            for (String line : lines) {
                if (!isContentRead) {
                    // parse title
                    if (line.contains("<a") && line.contains("artTitle")) {
                        currentTitle = line.substring(line.indexOf(">") + 1, line.indexOf("</a>"));
                        logger.info("[Title] " + currentTitle);
                    } else if (!currentTitle.isEmpty()) {
                        // parse date
                        if (line.contains("<time") && line.contains("datetime")) {
                            int datetimeIndex = line.indexOf("datetime=\"") + 10;
                            currentDate = LocalDateTime.parse(line.substring(datetimeIndex, datetimeIndex + 16), dateFormatter);
                            logger.info("[Date] " + currentDate.toString());
                        // parse content
                        } else if (line.contains("<div") && line.contains("artContentShort")) {
                            // content begins
                            if (!line.contains("</div>")) {
                                currentContent += line.substring(line.indexOf(">") + 1).trim();
                                isContentRead = true;
                            // whole content
                            } else {
                                currentContent = line.substring(line.indexOf(">") + 1, line.indexOf("</div>"));
                                logger.info("[Content] " + currentContent);
                                articles.add(new Article(currentTitle, currentContent, currentDate));
                                currentContent = "";
                                currentTitle = "";
                                currentDate = null;
                            }
                        }
                    }
                } else {
                    // parse content
                    // content continues
                    if (!line.contains("</div>")) {
                        currentContent += line.trim();
                    // content ends
                    } else {
                        currentContent += line.substring(0, line.indexOf("</div>")).trim();
                        isContentRead = false;
                        articles.add(new Article(currentTitle, currentContent, currentDate));
                        logger.info("[Content] " + currentContent);
                        currentContent = "";
                        currentTitle = "";
                        currentDate = null;
                    }
                }
            }
            createResult(articles);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void parseHTMLWithRegExp() {
        logger.info("[Parsing using reg exp] ");
        try {
            List<String> lines = Files.readAllLines(calculatePath());

            List<Article> articles = new ArrayList<>();
            String currentTitle = "";
            String currentContent = "";
            LocalDateTime currentDate = null;
            boolean isContentRead = false;

            for (String line : lines) {
                Matcher mTitle = HTMLPatterns.TITLE.matcher(line);
                Matcher mContentWhole = HTMLPatterns.CONTENT_WHOLE.matcher(line);
                Matcher mContentBegin = HTMLPatterns.CONTENT_BEGIN.matcher(line);
                Matcher mContentEnd = HTMLPatterns.CONTENT_END.matcher(line);
                Matcher mDate = HTMLPatterns.DATETIME.matcher(line);

                if (!isContentRead) {
                    // parse title
                    if (mTitle.find()) {
                        currentTitle = mTitle.group(1);
                        logger.info("[Title] " + currentTitle);
                    } else if (!currentTitle.isEmpty()) {
                        // parse date
                        if (mDate.find()) {
                            currentDate = LocalDateTime.parse(mDate.group(1), dateFormatter);
                            logger.info("[Date] " + currentDate.toString());
                        // parse content
                        // content whole
                        } else if (mContentWhole.find()) {
                            currentContent = mContentWhole.group(1);
                            logger.info("[Content] " + currentContent);
                            articles.add(new Article(currentTitle, currentContent, currentDate));
                            currentContent = "";
                            currentTitle = "";
                            currentDate = null;
                        // content begins
                        } else if (mContentBegin.find()) {
                            currentContent += mContentBegin.group(1).trim();
                            isContentRead = true;
                        }
                    }
                } else {
                    // content continues
                    if (!mContentEnd.find()) {
                        currentContent += line.trim();
                    // content ends
                    } else {
                        currentContent += mContentEnd.group(1).trim();
                        isContentRead = false;
                        articles.add(new Article(currentTitle, currentContent, currentDate));
                        logger.info("[Content] " + currentContent);
                        currentContent = "";
                        currentTitle = "";
                        currentDate = null;
                    }
                }
            }
            createResult(articles);
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
