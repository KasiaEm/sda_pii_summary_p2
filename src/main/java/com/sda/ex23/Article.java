package com.sda.ex23;

import java.time.LocalDateTime;

public class Article {
    private String title;
    private String content;
    private LocalDateTime dateTime;

    public Article(String title, String content, LocalDateTime dateTime) {
        this.title = title;
        this.content = content;
        this.dateTime = dateTime;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }
}
