package com.sda.ex23;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.List;

public abstract class HTMLParser {
    private String fileName;
    private Logger logger = LogManager.getLogger(HTMLParser.class);

    public HTMLParser(String fileName) {
        logger.info("[Created parser for file] " + fileName);
        this.fileName = fileName;
    }

    public ParsingResult createResult(List<Article> articles) {
        logger.info("[Saved articles] " + articles.size());
        return new ParsingResult(articles, LocalDateTime.now());
    }

    public Path calculatePath() throws URISyntaxException {
        return Paths.get(getClass().getClassLoader().getResource(fileName).toURI());
    }
}
