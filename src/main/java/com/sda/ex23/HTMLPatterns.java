package com.sda.ex23;

import java.util.regex.Pattern;

public class HTMLPatterns {
    final static Pattern TITLE = Pattern.compile("<a.*class=\".*artTitle.*\".*>(.*)</a>");
    final static Pattern CONTENT_WHOLE = Pattern.compile("<div.*class=\".*artContentShort.*\".*>(.*)</div>");
    final static Pattern CONTENT_BEGIN = Pattern.compile("<div.*class=\".*artContentShort.*\".*>(.*)");
    final static Pattern CONTENT_END = Pattern.compile("(.*)</div>");
    final static Pattern DATETIME = Pattern.compile("<time.*datetime.*=.*\"(.*)\".*>");
}
