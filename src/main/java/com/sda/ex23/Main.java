package com.sda.ex23;

public class Main {
    public static void main(String[] args) {
        SimpleHTMLParser parser = new SimpleHTMLParser("News.html");
        //parser.parseHTML();
        parser.parseHTMLWithRegExp();
        AdvancedHTMLParser advancedParser = new AdvancedHTMLParser("News2.html");
        //advancedParser.parseHTML();
        //advancedParser.parseHTMLWithRegExp();
    }
}
