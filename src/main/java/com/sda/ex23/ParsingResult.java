package com.sda.ex23;

import java.time.LocalDateTime;
import java.util.List;

public final class ParsingResult {
    private final List<Article> articles;
    private final LocalDateTime date;

    public ParsingResult(List<Article> articles, LocalDateTime date) {
        this.articles = articles;
        this.date = date;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public LocalDateTime getDate() {
        return date;
    }
}
