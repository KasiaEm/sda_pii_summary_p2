package com.sda.ex24;

public class DBException extends Exception {
    public DBException(String message){
        super(message);
    }
}
