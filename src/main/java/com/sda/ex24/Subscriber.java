package com.sda.ex24;

public interface Subscriber {
    void update(Chat chat);
}
