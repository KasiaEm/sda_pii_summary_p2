package com.sda.ex28;

import com.sda.ex28.operations.ImageOperation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Stack;

public class ImageEditor {
    private Logger logger = LogManager.getLogger(ImageEditor.class);
    private BufferedImage imageTemp;
    private Stack<OperationDone> operationsDone;

    public void loadImage(String filename){
        try {
            if(filename ==null || filename.isEmpty()){
                logger.warn("Filename is empty.");
            } else {
                Path path = Paths.get(getClass().getClassLoader().getResource(filename).toURI());
                imageTemp = ImageIO.read(new File(path.toUri()));
                operationsDone = new Stack<>();
                logger.info("Successfuly loaded image " + filename);
            }
        } catch (URISyntaxException | IOException e) {
            logger.error("Image not found.");
            e.printStackTrace();
        }
    }

    public void saveImage(){
        try {
            if(imageTemp==null){
                logger.warn("There's no image to save.");
            } else {
                ImageIO.write(imageTemp, "jpg", new File("D://tmp.jpg"));
                logger.info("Successfuly saved image.");
            }
        } catch (IOException e) {
            logger.error("Error saving image.");
            e.printStackTrace();
        }
    }

    public void perform(ImageOperation imageOperation){
        ColorModel cm = imageTemp.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = imageTemp.copyData(null);
        BufferedImage stateBefore = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
        //operation pushed to stack
        operationsDone.push(new OperationDone(stateBefore, imageOperation));
        //operation execute
        imageOperation.execute(imageTemp);
    }

    public void undo(){
        if(operationsDone.empty()){
            logger.warn("No operations to undo.");
        } else {
            OperationDone undone = operationsDone.pop();
            imageTemp = undone.getImageBefore();
            logger.info("Operation " + undone.getImageOperation() + " undone.");
        }
    }
}
