package com.sda.ex28;

import com.sda.ex28.operations.ImageOperation;

import java.awt.image.BufferedImage;

public class OperationDone {
    private BufferedImage imageBefore;
    private ImageOperation imageOperation;

    public OperationDone(BufferedImage imageBefore, ImageOperation imageOperation) {
        this.imageBefore = imageBefore;
        this.imageOperation = imageOperation;
    }

    public BufferedImage getImageBefore() {
        return imageBefore;
    }

    public ImageOperation getImageOperation() {
        return imageOperation;
    }
}
