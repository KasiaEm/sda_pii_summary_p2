package com.sda.ex28.operations;

import java.awt.image.BufferedImage;

public interface ImageOperation {
    void execute(BufferedImage image);
}
